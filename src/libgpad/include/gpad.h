/**
 * gpad.h
 *
 * This file is part of gpademu.
 *
 * Gpademu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gpademu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gpademu.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _MAIN_H
#define _MAIN_H

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#define GPAD_VERSION_MAJOR 1
#define GPAD_VERSION_MINOR 0
#define GPAD_VERSION TOSTRING(GPAD_VERSION_MAJOR) \
    "." TOSTRING(GPAD_VERSION_MINOR)

enum GPAD_ENUM {
    GPAD_OK,
    GPAD_WRITE_FAIL,
    GPAD_IOCTL_FAIL,
    GPAD_UINPUT_ACCESS_FAIL,
    GPAD_UIDEV_CREATE_FAIL
};

int init_uidev(void);
int close_uidev(void);
int write_uinput_event(void);

#endif /* _MAIN_H */
