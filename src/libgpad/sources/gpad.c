/**
 * gpad.c
 *
 * This file is part of gpademu.
 *
 * Gpademu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gpademu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gpademu.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <linux/input.h>
#include <linux/uinput.h>

#include "gpad.h"


static struct uinput_user_dev uidev;
static struct input_event ev;
static unsigned char toggle = 0;
static int fd;

/**
 * Initialize a new uinput device
 */
int init_uidev(void)
{
    fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if (fd < 0) {
        //printf("Opening of uinput failed!\n
        //Have you loaded uinput kernel module?\n");
        return GPAD_UINPUT_ACCESS_FAIL;
    }
    ioctl(fd, UI_SET_EVBIT, EV_KEY); //setting Gamepad keys
    ioctl(fd, UI_SET_KEYBIT, BTN_A);
    ioctl(fd, UI_SET_KEYBIT, BTN_B);
    ioctl(fd, UI_SET_KEYBIT, BTN_X);
    ioctl(fd, UI_SET_KEYBIT, BTN_Y);
    ioctl(fd, UI_SET_KEYBIT, BTN_TL);
    ioctl(fd, UI_SET_KEYBIT, BTN_TR);
    ioctl(fd, UI_SET_KEYBIT, BTN_TL2);
    ioctl(fd, UI_SET_KEYBIT, BTN_TR2);
    ioctl(fd, UI_SET_KEYBIT, BTN_START);
    ioctl(fd, UI_SET_KEYBIT, BTN_SELECT);
    ioctl(fd, UI_SET_KEYBIT, BTN_THUMBL);
    ioctl(fd, UI_SET_KEYBIT, BTN_THUMBR);
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_UP);
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_DOWN);
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_LEFT);
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_RIGHT);
    ioctl(fd, UI_SET_EVBIT, EV_ABS);
    ioctl(fd, UI_SET_ABSBIT, ABS_X);
    ioctl(fd, UI_SET_ABSBIT, ABS_Y);
    ioctl(fd, UI_SET_ABSBIT, ABS_RX);
    ioctl(fd, UI_SET_ABSBIT, ABS_RY);

    memset(&uidev, 0, sizeof(uidev));
    snprintf(uidev.name, UINPUT_MAX_NAME_SIZE, "gpademu_gamepad");

    uidev.id.bustype = BUS_USB;
    uidev.id.vendor  = 0x3;
    uidev.id.product = 0x3;
    uidev.id.version = 2;
    uidev.absmax[ABS_X] = 512;
    uidev.absmin[ABS_X] = -512;
    uidev.absfuzz[ABS_X] = 0;
    uidev.absflat[ABS_X] = 15;
    uidev.absmax[ABS_Y] = 512; 
    uidev.absmin[ABS_Y] = -512;
    uidev.absfuzz[ABS_Y] = 0;
    uidev.absflat[ABS_Y] = 15;
    uidev.absmax[ABS_RX] = 512;
    uidev.absmin[ABS_RX] = -512;
    uidev.absfuzz[ABS_RX] = 0;
    uidev.absflat[ABS_RX] = 16;
    uidev.absmax[ABS_RY] = 512;
    uidev.absmin[ABS_RY] = -512;
    uidev.absfuzz[ABS_RY] = 0;
    uidev.absflat[ABS_RY] = 16;
    if(write(fd, &uidev, sizeof(uidev)) < 0) //writing settings
    {
        //printf("error: write");
        return GPAD_WRITE_FAIL;
    }
    if(ioctl(fd, UI_DEV_CREATE) < 0) //writing ui dev create
    {
        //printf("error: ui_dev_create");
        return GPAD_UIDEV_CREATE_FAIL;
    }
    return GPAD_OK;
}

/**
 * Close uinput devic
 */
int close_uidev(void)
{
    if(ioctl(fd, UI_DEV_DESTROY) < 0)
    {
        //printf("error: ioctl");
        close(fd);
        return GPAD_IOCTL_FAIL;
    }
    close(fd);
    return GPAD_OK;
}

/**
 * Write events to the uinput device
 */
int write_uinput_event(void)
{
    //while(1)
    //{
    memset(&ev, 0, sizeof(struct input_event)); //setting the memory for event
    ev.type = EV_KEY;
    ev.code = BTN_X;
    ev.value = !toggle;
    toggle = !toggle;
    if(write(fd, &ev, sizeof(struct input_event)) < 0) //writing the key change
    {
        //printf("error: key-write");
        return GPAD_WRITE_FAIL;
    }
    memset(&ev, 0, sizeof(struct input_event)); //setting the memory for event
    ev.type = EV_ABS;
    ev.code = ABS_X;
    ev.value = toggle == 1 ? 256 : 0;
    if(write(fd, &ev, sizeof(struct input_event)) < 0) //writing the thumbstick change
    {
        //printf("error: thumbstick-write");
        return GPAD_WRITE_FAIL;
    }

    memset(&ev, 0, sizeof(struct input_event));
    ev.type = EV_SYN;
    ev.code = SYN_REPORT;
    ev.value = 0;
    if(write(fd, &ev, sizeof(struct input_event)) < 0) //writing the sync report
    {
        //printf("error: sync-report");
        return GPAD_WRITE_FAIL;
    }
    //} // while-loop
    return GPAD_OK;
}

// vim: et sw=4 ts=4 :
