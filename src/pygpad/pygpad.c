/**
 * pygpad.c
 *
 * This file is part of gpademu.
 *
 * Gpademu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gpademu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gpademu.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <Python.h>

#include "gpad.h"

static PyObject* wrap_init_uidev(PyObject* self, PyObject* args)
{
    int status = init_uidev();
    return Py_BuildValue("i", status);
}

static PyObject* wrap_write_uinput_event(PyObject* self, PyObject* args)
{
    int status = write_uinput_event();
    return Py_BuildValue("i", status);
}

static PyObject* wrap_close_uidev(PyObject* self, PyObject* args)
{
    int status = close_uidev();
    return Py_BuildValue("i", status);
}

static PyObject* version(PyObject* self, PyObject* args)
{
    /* gpad python library version */
    return Py_BuildValue("s", "v1.0");
}

static PyMethodDef ModuleMethods[] =
{
    {
        "init",
        wrap_init_uidev,
        METH_NOARGS,
        "Initialize uinput device\n\
        Take no arguments\n\
        Return an integer which represents an \
index of GPAD_ENUM in 'gpad.h'"
    },
    {
        "write_event",
        wrap_write_uinput_event,
        METH_NOARGS,
        "Write an event to the device\n\
        Take no arguments\n\
        Return an integer which represents an \
index of GPAD_ENUM in 'gpad.h'"
    },
    {
        "close",
        wrap_close_uidev,
        METH_NOARGS,
        "Close uinput device\n\
        Take no arguments\n\
        Return an integer which represents an \
index of GPAD_ENUM in 'gpad.h'"
    },
    {
        "version",
        (PyCFunction) version,
        METH_NOARGS,
        "Return the library version"
    },
    { NULL, NULL, 0, NULL } /* sentinel */
};

static struct PyModuleDef gpad_definition =
{
    PyModuleDef_HEAD_INIT,
    "gpad",
    "Uinput virtual gamepad",
    -1,
    ModuleMethods
};

PyMODINIT_FUNC
// must be PyInit_[module name]
PyInit_gpad(void)
{
    Py_Initialize();
    return PyModule_Create(&gpad_definition);
}
