/**
 * test.c
 *
 * This file is part of gpademu.
 *
 * Gpademu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gpademu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gpademu.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "gpad.h"

int main(size_t argc, char ** argv, char ** envp)
{
    int status;
    status = init_uidev();
    if(status != GPAD_OK)
    {
        printf("Init failed with: %d\n", status);
        return 1;
    }

    printf("Starting test\n");
    while (1)
    {
        status = write_uinput_event();
        if(status != GPAD_OK)
        {
            printf("Write failed with: %d\n", status);
            break;
        }
    }

    status = close_uidev();
    if(status != GPAD_OK)
    {
        printf("Close failed with: %d\n", status);
        return 1;
    }
    return 0;
}
