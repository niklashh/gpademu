# GPADEMU

Gpademu is a gamepad emulator library.

### Dependencies

- python3 headers
- cmake

### Installation

##### Libgpad

- Go to `src` folder
- Create a `build` folder

```shell
mkdir build
```

- Go to the `build` folder and compile the library and test(s)

```shell
cd build
cmake ..
make
```

- Install the library (requires super-user permissions)

```shell
make install
```

##### Python wrapper library

- Go to `src/pygpad` and create another build folder
- Go to the `build` folder
- Compile the python wrapper

```shell
cmake ..
make
```

### Running tests

Enable uinput kernel module (as super-user
```shell
modprobe uinput
```

##### Using C

- Go to `src/build/test`
- Then run the following command (as super-user)

```shell
./test
```

> Libgpad requires access to `/dev/uinput`

##### Using Python3

> The `pygpad`'s build process created a folder `build` (in `src/pygpad/build`)
> with the python library in it

- Go to the python library folder `src/pygpad/build/build`

```shell
cd lib*
python -ic 'import gpad'
```

Now you can use all the function from the library with Python!

> To see the available functions using the Python interpreter, write `gpad.`
> and then press `[TAB]`
